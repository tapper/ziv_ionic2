import {Component, OnInit} from '@angular/core';
import { NavController } from 'ionic-angular';
import {Config} from "../../services/config";
import {SendToServerService} from "../../services/send_to_server";
import {AboutPage} from "../about/about";
import {PhonebookPage} from "../phonebook/phonebook";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{

  public CategoryArray:any;
  public mainCategories:any;
  public mainDataArray:any;
  public hostUrl;
  constructor(public navCtrl: NavController,public Server:SendToServerService,public Settings:Config) {
    this.hostUrl = this.Settings.LaravelHost;
  }


    ngOnInit()
    {
        this.Server.getMainData('getMainData').then((data: any) => { this.Settings.mainDataArray = data;});
    }

    navigatePage(page)
    {
        switch(page) {
            case 0:
                this.navCtrl.push(AboutPage);
                break;
            case 1:
                this.navCtrl.push(PhonebookPage);
                break;
        }
    }

}
